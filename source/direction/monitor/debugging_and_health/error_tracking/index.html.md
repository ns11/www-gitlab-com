---
layout: markdown_page
title: "Category Vision - Error Tracking"
---

- TOC
{:toc}

## Error Tracking
Error Tracking allows GitLab to receive and act on application errors. We should improve the workflow between GitLab and Sentry, to allow users to more easily work with and interact with errors.

We should:
* Allow users to configure a Sentry project to a GitLab project (URL, token)
* List the errors for their project
* View the key details of an error

This will lay the foundation to allow us to also embed errors within an Issue and Merge Requests, as well as additional features for determining analytics around the cause of an error.

[Sentry has also started adding source code awareness](https://blog.sentry.io/2017/05/01/release-commits.html)

## What's Next & Why
Not yet, but accepting merge requests to this document.

## Maturity Plan
* [MVC Epic](https://gitlab.com/groups/gitlab-org/-/epics/169)

## Competitive Landscape
Not yet, but accepting merge requests to this document.

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
