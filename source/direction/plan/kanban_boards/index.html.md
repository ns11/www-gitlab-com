---
layout: markdown_page
title: "Category Vision - Kanban Boards"
---

- TOC
{:toc}

## Kanban Boards
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

GitLab has powerful and flexible kanban boards (which we call issue boards) to help teams prioritize, manage, and track work execution. They are geared towards software development teams, but are also flexible enough for other teams in your organization to manage any type of tasks requiring tracking.

Issue boards are intended for planning sprints, tracking execution during sprints, and even evaluation after the fact, including integrating with [value stream management](https://gitlab.com/groups/gitlab-org/-/epics/668) with [custom workflows](https://gitlab.com/groups/gitlab-org/-/epics/505) and [burndown/up charts](https://gitlab.com/groups/gitlab-org/-/epics/372).

## Beyond Agile workflows

GitLab issue boards have evolved beyond just allowing teams to track Agile workflows. You can also use them for sprint planning, cross-functional planning, and even user assignment visibility. Since we already have three types of lists in GitLab (labels, milestones, and assignees), users/customers have been finding new ways to use issue boards. We have to be careful as we evolve issue boards in the future that we don't let this immense flexibility create too much complications that hinders users from easily using issue boards. At the same time, we want to still retain that level of power so that users can do planning the way they want. Below are some ways we are considering to further refine the design. But in general, our vision of issue boards is to allow teams to organize and track their work in most flexible part of GitLab.

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

We are now focused on helping users better use the existing powerful capabilities of issue boards. In particular, we are improving the design of issue boards with [more opinionated workflows](https://gitlab.com/groups/gitlab-org/-/epics/500). We are also 
[integrating epics into issue boards](https://gitlab.com/groups/gitlab-org/-/epics/328).

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The top competitor in this space is Atlassian's Jira, who are entrenched in many enterprise organizations who need an Agile/Kanban board solution. Atlassian also bought Trello, which is another significant player in this space, which has emphasized usability and being able to abstract out underlying software implementation details of an Agile sprint, to just simple task planning with a board interface.

Jira's and Trello's boards have inspired us to further refine and make our boards even more usable. In particular, we have the following ideas sketched and scoped out, including doing a lot more right in the board itself, without leaving it:
- [Combine board config and board filter for milestones](https://gitlab.com/groups/gitlab-org/-/epics/500)
- [Rows and columns design of boards - Remove mixed lists boards](https://gitlab.com/groups/gitlab-org/-/epics/616)
- [Epic swimlanes and filter in boards](https://gitlab.com/groups/gitlab-org/-/epics/328)
- [Edit issues without leaving board](https://gitlab.com/groups/gitlab-org/-/epics/383)

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

Similar to [Project Management](../project_management/index.html.md), the analyst landscape is focused on enteprise agile planning and value stream management. Kanban boards are a means to further make these processes more refined and efficient. See:

- [Agile Portfolio Management](../agile_portfolio_management/index.html.md)
- [Value Stream Management](../value_stream_management/index.html.md)

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

Customers really want a way to do workflows inside GitLab. They see the opportunity with label lists, but feel the pain of the shortcomings. So that's why 
[Group board with custom workflow](https://gitlab.com/groups/gitlab-org/-/epics/424) is so crucial to achieve this.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Users in general want custom workflows that work, with no pain, and weird behavior. They really want first class workflow states integrated into boards. 
So [Group board with custom workflow](https://gitlab.com/groups/gitlab-org/-/epics/424) and 
[Custom workflow per group](https://gitlab.com/groups/gitlab-org/-/epics/364) need to be achieved to satisfy that.

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

GitLab team-members want more flexibility to do even more with boards. In particular, GitLab team-members really want to take advantage of epics integrated in boards. 
[Epic swimlanes](https://gitlab.com/groups/gitlab-org/-/epics/328) are thus crucial here. 

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

The most important items are further refining the board with more opinionated designs, adding swimlanes, and allowing users to do more without leaving the board itself.

- [Combine board config and board filter for milestones](https://gitlab.com/groups/gitlab-org/-/epics/500)
- [Rows and columns design of boards - Remove mixed lists boards](https://gitlab.com/groups/gitlab-org/-/epics/616)
- [Epic swimlanes and filter in boards](https://gitlab.com/groups/gitlab-org/-/epics/328)
- [Edit issues without leaving board](https://gitlab.com/groups/gitlab-org/-/epics/383)
